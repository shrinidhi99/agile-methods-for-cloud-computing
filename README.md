# Agile method supported by cloud computing
---
![Agile life cycle model](https://i1.wp.com/number8.com/wp-content/uploads/2017/03/Agile-Software-Development.png?fit=607%2C597&ssl=1 "Agile life cycle model")

Agile method and cloud computing is the new combination that is very much necessary to adopt in the current world software engineering projects. In this paper, we have shown how introducing cloud computing in agile framework gives whole lot of benefits and why agile model is chosen over others with foolproof case studies.